package com.android.shen.miniprojet_shen_xinyue;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private Intent itLogin;
    private ImageView viewPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //to check the type of user, and choose the main page according to the type
        itLogin = getIntent();
        if(itLogin.getIntExtra("usertype", 100) == 100)
            navigation_admin();
        else
            navigation();
    }

    //user main page
    public void navigation() {
        //toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        setContentView(R.layout.activity_main);



        final NavigationView navigationView = findViewById(R.id.nv);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id){
                    case R.id.profil :
                        navigationChoice(ProfilActivity.class);
                        break;
                    case R.id.listeDesMuseesARouen :
                        navigationChoice(ListMuseeActivity.class);
                        break;
                    case R.id.mesVisites :
                        navigationChoice(VisitActivity.class);
                        break;
                    case R.id.evaluerUneVisite :
                        navigationChoice(EvaluerActivity.class);
                        break;
                    case R.id.statistiques :
                        navigationChoice(StatistiqueActivity.class);
                        break;
                    case  R.id.quitter :
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                        break;
                }

                return true;
            }
        });

        //add header to the Navigation bar
        View header= LayoutInflater.from(this).inflate(R.layout.header, null);
        navigationView.addHeaderView(header);
        //header.setVisibility(View.VISIBLE);
        TextView tvMusee = header.findViewById(R.id.info_user);
        tvMusee.setText("Nombre de Musées visités: " + String.valueOf(itLogin.getIntExtra("nbMusee", 0)));
        viewPhoto = findViewById(R.id.ic_person);
        //viewPhoto.setImageBitmap();


        drawerLayout = findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle
                = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    //admin main page
    public void navigation_admin() {
        setContentView(R.layout.activity_admin_main);


        //toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        final NavigationView navigationView = findViewById(R.id.nv_admin);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id){
                    case R.id.carte:
                        navigationChoice(AfficherActivity.class);
                        break;
                    case R.id.statistiques_admin :
                        navigationChoice(StatistiqueActivity.class);
                        break;
                    case  R.id.quitter :
                        android.os.Process.killProcess(android.os.Process.myPid());
                        System.exit(1);
                        break;
                }

                return true;
            }
        });

        //add header to the Navigation bar
        View header= LayoutInflater.from(this).inflate(R.layout.header, null);
        navigationView.addHeaderView(header);
        TextView tvAdmin = header.findViewById(R.id.info_user);
        tvAdmin.setText("Bienvenue Admin");
        viewPhoto = findViewById(R.id.ic_person);


        drawerLayout = findViewById(R.id.drawer_admin);
        ActionBarDrawerToggle actionBarDrawerToggle
                = new ActionBarDrawerToggle(this, drawerLayout,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void navigationChoice(Class choice) {
        Intent intent = new Intent(this, choice);
        if (itLogin.getStringExtra("nom") != null){
            intent.putExtra("getnom", itLogin.getStringExtra("nom"));
            intent.putExtra("getprenom", itLogin.getStringExtra("prenom"));

            startActivity(intent);
        }
        else
            System.out.println("parameter in main nom is null");
    }
}
