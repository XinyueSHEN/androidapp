package com.android.shen.miniprojet_shen_xinyue;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TextView;

import com.android.shen.miniprojet_shen_xinyue.dao.VisitBDD;
import com.android.shen.miniprojet_shen_xinyue.dto.Visit;

import java.util.List;

public class EvaluerActivity extends AppCompatActivity {
    TableLayout tableLayout;
    VisitBDD visitBDD;
    TextView getDate;
    TextView getNom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluer);
        getLastVisit();
    }

    public void getLastVisit(){
        visitBDD = new VisitBDD(this);
        visitBDD.open();
        List<Visit> visits = visitBDD.getVisits();

        getDate = findViewById(R.id.date_visit);
        getNom = findViewById(R.id.nom_visit);
        getDate.setText(String.valueOf(visits.get(visits.size()).getDate()));
        getNom.setText(String.valueOf(visits.get(visits.size()).getTitle()));
    }
}
