package com.android.shen.miniprojet_shen_xinyue.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.shen.miniprojet_shen_xinyue.dto.Person;


public class PersonBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "example.db";
    private static final String TABLE_PERSON = "persons";


    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NOM = "Nom";
    private static final int NUM_COL_NOM = 1;
    private static final String COL_PRENOM = "Prenom";
    private static final int NUM_COL_PRENOM = 2;
   // private static final String COL_TEL = "Tel";
   // private static final int NUM_COL_TEL = 3;
   // private static final String COL_VILLE = "Ville";
   // private static final int NUM_COL_VILLE = 4;
    private static final String COL_NBMUSEE = "NbMusee";
    private static final int NUM_COL_NBMUSEE = 3;
    private static final String COL_EMAIL = "Email";
    private static final int NUM_COL_EMAIL = 4;
    private static final String COL_PASSWORD = "Password";
    private static final int NUM_COL_PASSWORD = 5;
    private static final String COL_USERTYPE = "UserType";
    private static final int NUM_COL_USERTYPE = 6;
    private static final String COL_PHOTO = "Photo";
    private static final int NUM_COL_PHOTO = 7;

    private SQLiteDatabase bdd;

    private MyBase myBase;

    public PersonBDD(Context context){
        myBase = new MyBase(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open () {
        bdd = myBase.getWritableDatabase();
    }

    public void close () {
        bdd.close();
    }

    public SQLiteDatabase getBdd(){
        return bdd;
    }

    public long ajouterPerson(Person person){
        ContentValues values = new ContentValues();
        values.put(COL_NOM, person.getNom());
        values.put(COL_PRENOM, person.getPrenom());
        values.put(COL_EMAIL, person.getEmail());
        values.put(COL_PASSWORD, person.getPassword());
        values.put(COL_NBMUSEE, person.getNbMusee());
        //values.put(COL_VILLE, person.getVille());
        //values.put(COL_TEL, person.getTel());
        values.put(COL_USERTYPE, person.getUserType());
        values.put(COL_PHOTO, person.getPhoto());
        return bdd.insert(TABLE_PERSON, null, values);
    }

    public int majPerson(int id, Person person) {
        ContentValues values = new ContentValues();
        values.put(COL_NOM, person.getNom());
        values.put(COL_PRENOM, person.getPrenom());
        values.put(COL_EMAIL, person.getEmail());
        values.put(COL_PASSWORD, person.getPassword());
        values.put(COL_NBMUSEE, person.getNbMusee());
        values.put(COL_USERTYPE, person.getUserType());
        values.put(COL_PHOTO, person.getPhoto());
        return bdd.update(TABLE_PERSON, values, COL_ID + " = " + id, null);
    }

    public int supprimerPersonPersonID (int id){
        return bdd.delete(TABLE_PERSON, COL_ID + " = " + id, null);
    }

    public Person getPersonParNom(String nom) {
        Cursor c = bdd.query(TABLE_PERSON, new String[] {
                COL_ID, COL_NOM, COL_PRENOM, COL_NBMUSEE, COL_EMAIL, COL_PASSWORD, COL_USERTYPE, COL_PHOTO
        }, COL_NOM + " LIKE \"" + nom + "\"", null, null, null, null);
        return cursorToPerson(c);
    }

    public Person getPersonParEMAIL(String email, String password) {
        Cursor c = bdd.query(TABLE_PERSON, new String[] {
                COL_ID, COL_NOM, COL_PRENOM, COL_NBMUSEE, COL_EMAIL, COL_PASSWORD, COL_USERTYPE, COL_PHOTO
        }, COL_EMAIL + " LIKE \"" + email + "\" AND " + COL_PASSWORD + " LIKE \"" + password + "\"",
                null, null, null, null);
        return cursorToPerson(c);
    }

    public Person cursorToPerson (Cursor c){
        if (c.getCount() == 0)
            return null;
        c.moveToFirst();
        Person person = new Person();
        person.setId(c.getInt(NUM_COL_ID));
        person.setNom(c.getString(NUM_COL_NOM));
        person.setPrenom(c.getString(NUM_COL_PRENOM));
        person.setEmail(c.getString(NUM_COL_EMAIL));
        person.setPassword(c.getString(NUM_COL_PASSWORD));
        person.setNbMusee(c.getInt(NUM_COL_NBMUSEE));
        //person.setVille(c.getString(NUM_COL_VILLE));
        //person.setTel(c.getString(NUM_COL_TEL));
        person.setUserType(c.getInt(NUM_COL_USERTYPE));
        person.setPhoto(c.getString(NUM_COL_PHOTO));
        c.close();
        return person;
    }
}
