package com.android.shen.miniprojet_shen_xinyue.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.shen.miniprojet_shen_xinyue.dto.Visit;

import java.util.LinkedList;
import java.util.List;

public class VisitBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "example.db";
    private static final String TABLE_VISIT = "visits";


    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_DISTANCE = "Distance";
    private static final int NUM_COL_DISTANCE = 1;
    private static final String COL_DUREE= "Duree";
    private static final int NUM_COL_DUREE = 2;
    private static final String COL_TITLE = "Title";
    private  static final int NUM_COL_TITLE = 3;
    private static final String COL_DATE = "Date";
    private static final int NUM_COL_DATE = 4;

    private SQLiteDatabase bdd;

    private MyBase myBase;

    public VisitBDD(Context context){
        myBase = new MyBase(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open () {
        bdd = myBase.getWritableDatabase();
    }

    public void close () {
        bdd.close();
    }

    public SQLiteDatabase getBdd(){
        return bdd;
    }

    public long ajouterVisit(Visit visit){
        ContentValues values = new ContentValues();
        values.put(COL_DISTANCE, visit.getDistance());
        values.put(COL_DUREE, visit.getDuree());
        values.put(COL_TITLE, visit.getTitle());
        values.put(COL_DATE,visit.getDate().toString());

        return bdd.insert(TABLE_VISIT, null, values);
    }

    public int majVisit(int id, Visit visit) {
        ContentValues values = new ContentValues();
        values.put(COL_DISTANCE, visit.getDistance());
        values.put(COL_DUREE, visit.getDuree());
        values.put(COL_TITLE, visit.getTitle());
        values.put(COL_DATE,visit.getDate().toString());
        return bdd.update(TABLE_VISIT, values, COL_ID + " = " + id, null);
    }

    public int supprimerVisitVisitID (int id){
        return bdd.delete(TABLE_VISIT, COL_ID + " = " + id, null);
    }

    /*public Visit getVisitParNom(String nom) {
        Cursor c = bdd.query(TABLE_VISIT, new String[] {
                COL_ID, COL_DISTANCE, COL_DUREE
        }, COL_ + " LIKE \"" + nom + "\"", null, null, null, null);
        return cursorToVisit(c);
    }*/

    public List<Visit> getVisits() {
        Cursor c = bdd.query(TABLE_VISIT, new String[] {
                COL_ID, COL_DISTANCE, COL_DUREE, COL_TITLE, COL_DATE
        }, null, null, null, null, null);
        return cursorToVisit(c);
    }

    public List<Visit> cursorToVisit(Cursor c){
        List<Visit> visits = new LinkedList<>();
        if (c.getCount() == 0)
            return null;
        c.moveToFirst();
        if(!c.isLast()){
            Visit visit = new Visit();
            visit.setId(c.getInt(NUM_COL_ID));
            visit.setDistance(c.getDouble(NUM_COL_DISTANCE));
            visit.setDuree(c.getDouble(NUM_COL_DUREE));
            c.moveToNext();
            visits.add(visit);
        }

        c.close();
        return visits;
    }
}
