package com.android.shen.miniprojet_shen_xinyue.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.shen.miniprojet_shen_xinyue.dto.Museum;

public class MuseumBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "example.db";
    private static final String TABLE_MUSEUM = "museums";


    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_DISTANCE = "Distance";
    private static final int NUM_COL_DISTANCE = 1;
    private static final String COL_DUREE= "Duree";
    private static final int NUM_COL_DUREE = 2;

    private SQLiteDatabase bdd;

    private MyBase myBase;

    public MuseumBDD(Context context){
        myBase = new MyBase(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open () {
        bdd = myBase.getWritableDatabase();
    }

    public void close () {
        bdd.close();
    }

    public SQLiteDatabase getBdd(){
        return bdd;
    }

    public long ajouterMuseum(Museum museum){
        ContentValues values = new ContentValues();
        //values.put(COL_DISTANCE, museum.);
        //values.put(COL_DUREE, museum.get);

        return bdd.insert(TABLE_MUSEUM, null, values);
    }

    public int majMuseum(int id, Museum museum) {
        ContentValues values = new ContentValues();
        //values.put(COL_DISTANCE, museum.);
        //values.put(COL_DUREE, museum.get);


        return bdd.update(TABLE_MUSEUM, values, COL_ID + " = " + id, null);
    }

    public int supprimerMuseumMuseumID (int id){
        return bdd.delete(TABLE_MUSEUM, COL_ID + " = " + id, null);
    }

    /*public Museum getMuseumParNom(String nom) {
        Cursor c = bdd.query(TABLE_MUSEUM, new String[] {
                COL_ID, COL_DISTANCE, COL_DUREE
        }, COL_ + " LIKE \"" + nom + "\"", null, null, null, null);
        return cursorToVisit(c);
    }*/


    public Museum cursorToVisit(Cursor c){
        if (c.getCount() == 0)
            return null;
        c.moveToFirst();
        Museum museum = new Museum();
        museum.setId(c.getInt(NUM_COL_ID));
        //museum.setDistance(c.getDouble(NUM_COL_DISTANCE));
        //museum.setDuree(c.getDouble(NUM_COL_DUREE));


        c.close();
        return museum;
    }
}
