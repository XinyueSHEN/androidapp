package com.android.shen.miniprojet_shen_xinyue.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyBase extends SQLiteOpenHelper {

    public MyBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE persons (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NOM TEXT NOT NULL, PRENOM TEXT NOT NULL, NBMUSEE TEXT NOT NULL, " +
                "EMAIL TEXT NOT NULL, PASSWORD TEXT NOT NULL, USERTYPE INTEGER NOT NULL, PHOTO TEXT) ");
        db.execSQL("INSERT INTO persons (NOM, PRENOM, NBMUSEE, EMAIL, PASSWORD, USERTYPE, PHOTO) " +
                "VALUES (\"Tom\",\"Cat\", 5, \"tomcat@email.com\", \"password\",0,\"Photo\")");
        db.execSQL("INSERT INTO persons (NOM, PRENOM, NBMUSEE, EMAIL, PASSWORD, USERTYPE, PHOTO) " +
                "VALUES (\"Jerry\",\"Mouse\", 3, \"j@\", \"password\",100,\"Photo\")");
        db.execSQL("INSERT INTO visits (DISTANCE, DUREE, TITLE, DATE) " +
                "VALUES (1455,568776.6, \"Maison natale Pierre Corneille\", \"7-1-2018\")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS persons");
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS visits");
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS museums");
        onCreate(db);
    }
}
