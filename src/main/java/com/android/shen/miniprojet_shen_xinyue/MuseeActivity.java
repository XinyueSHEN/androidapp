package com.android.shen.miniprojet_shen_xinyue;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.shen.miniprojet_shen_xinyue.dao.VisitBDD;
import com.android.shen.miniprojet_shen_xinyue.dto.Visit;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MuseeActivity extends AppCompatActivity {
    private TableLayout tb;
    private MuseeActivity.UIHandler UIhandler;
    private Button btnAdd;
    private VisitBDD visitBDD;
    private Visit visit;
    private TextView getHeure;
    private TextView getDate;
    private TextView getDistance;
    private TextView getDuree;

    Geocoder geocoder;
    LocationManager locationManager;
    Location location;
    private String locationProvider;
    private List<String> providerList;
    private Address address;
    private final String maps_key = "AIzaSyDbcQNQq4DsHSKCksE-7Q8zbVxZL7xnoSA";
    private final String base_url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=";
    private final String base_url_map = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    //This joins the parts of the URL together into one string.
    private String url;
    private String url_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musee);
        getHeure = findViewById(R.id.heure);
        getDate = findViewById(R.id.date);
        getDistance = findViewById(R.id.distance);
        getDuree = findViewById(R.id.duree);
        getDate.setText("07/01/2018");
        getHeure.setText("9:14");
        UIhandler = new MuseeActivity.UIHandler();
        tb = findViewById(R.id.list_info);
        btnAdd = findViewById(R.id.btn_add);


        //get current location
        locationInit();
        Intent itList = getIntent();
        itList.getStringExtra("title");
        String latitude = itList.getStringExtra("latitude");
        Double longitude = itList.getDoubleExtra("longitude", 0);
        //Washington,DC&destinations=New+York+City,NY&key=AIzaSyDbcQNQq4DsHSKCksE-7Q8zbVxZL7xnoSA
        url = base_url + location.getLatitude() + "," + location.getLongitude() + "&destinations="
                + latitude + "," + longitude + "&key=" + maps_key;
        url_map = base_url_map + location.getLatitude() + "," + location.getLongitude()
                + "&destination=" + latitude + "," + longitude + "&mode=transit&key=" + maps_key;

        //get Json info of the museum
        //make http request
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String webContent = response.body().string();
                if (!response.isSuccessful()) {
                    throw new IOException("Erreur: " + response);
                } else {
                    //create JSONObject to parse JSON to Java
                    //String str = new String("{ \"number\": [3, 4, 5, 6] }");
                    try {
                        JSONObject jsonObject = new JSONObject(new String(webContent));
                        Double distance = jsonObject.getJSONArray("rows").getJSONObject(0)
                                .getJSONArray("elements").getJSONObject(0)
                                .getJSONObject("distance").getDouble("value");

                        Double duree = jsonObject.getJSONArray("rows").getJSONObject(0)
                                .getJSONArray("elements").getJSONObject(0)
                                .getJSONObject("duration")
                                .getDouble("value");
                        //get the info of the visit from the Json file
                        getDistance.setText(String.valueOf(distance));
                        getDuree.setText(String.valueOf(duree));
                        visit = new Visit();
                        visit.setDistance(distance);
                        visit.setDuree(duree);


                        //send the string of title to the main thread
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        //bundle.putString("date", );
                        //bundle.putString("heure", );
                        //bundle.putString("distance",);
                        //bundle.putString("duree", );
                        msg.setData(bundle);
                        MuseeActivity.this.UIhandler.sendMessage(msg);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        });

        initButtun();
    }

    public void initButtun(){
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create a new visit if clicked

                visitBDD.open();

                visitBDD.ajouterVisit(visit);
                //Visit visitGet = visitBDD.get(nom.getText().toString());

                //if (visitGet != null) {
                //    Toast.makeText(MuseeActivity.this, visitGet.toString(),Toast.LENGTH_LONG).show();
                //}
            }
        });
    }

    //initialize location
    public void locationInit() {
        try {
            // get system service
            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            providerList = locationManager.getProviders(true);


            // check if GPS could get the location
            // check by GPS then network
            if (locationInitByGPS() || locationInitByNETWORK()) {
                // show location
                address = getAddress(getApplicationContext(),location.getLatitude(), location.getLongitude());
            } else {
                System.out.println("failed to get location");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // use GPS to get location
    public boolean locationInitByGPS() {
        List<String> providerList = locationManager.getProviders(true);
        if (providerList.contains(LocationManager.GPS_PROVIDER)) {
            System.out.println("=====GPS_PROVIDER=====");
            locationProvider = LocationManager.GPS_PROVIDER;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

            location = locationManager.getLastKnownLocation(locationProvider);
        }
        if (location != null) {
            return true;
        } else {
            return false;
        }
    }

    // use network to get location
    public boolean locationInitByNETWORK() {
        if (providerList.contains(LocationManager.NETWORK_PROVIDER)){
            System.out.println("=====NETWORK_PROVIDER=====");
            locationProvider = LocationManager.NETWORK_PROVIDER;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
            location = locationManager.getLastKnownLocation(locationProvider);
        }

        if (location != null) {
            return true;
        } else {
            return false;
        }
    }

    public Address getAddress(Context context, double latitude, double longitude) {
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    //to create the list in the main thread
    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            final String title = bundle.getString("row");
        }
    }
}
