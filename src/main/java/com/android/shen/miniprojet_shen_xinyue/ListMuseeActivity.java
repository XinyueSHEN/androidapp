package com.android.shen.miniprojet_shen_xinyue;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.shen.miniprojet_shen_xinyue.dto.Museum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ListMuseeActivity extends AppCompatActivity {

    private List<Museum> museums;
    private TableLayout tb;
    private UIHandler UIhandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_musee);
        UIhandler = new UIHandler();

        //get the list of the museums from the link in Json
        getFromJsonList();
    }


    protected void getFromJsonList() {
        //create the list of museums
        museums = new ArrayList<>();
        //list of museums
        tb = findViewById(R.id.list);


        //make http request
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("http://srvinfodev.esigelec.fr/musee.json")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String webContent = response.body().string();
                if (!response.isSuccessful()){
                    throw new IOException("Erreur: " +response);
                } else {
                    //create JSONObject to parse JSON to Java
                    //String str = new String("{ \"number\": [3, 4, 5, 6] }");
                    try {
                        JSONArray jsonarray = new JSONArray(new String(webContent));
                        JSONObject jsonObject;
                        for (int i = 0; i < jsonarray.length(); i++){
                            //get the list of museums from the Json file
                            jsonObject = jsonarray.getJSONObject(i);
                            Museum museumTampon = new Museum();
                            museumTampon.setTitle(jsonObject.getString("title"));
                            museumTampon.setDescription(jsonObject.getString("description"));
                            museumTampon.setTarifs(jsonObject.getString("tarifs"));
                            //museumTampon.setLatitude(Double.valueOf(jsonObject.getString("latitude")));
                            museumTampon.setLatitude(jsonObject.getString("latitude"));
                            museumTampon.setLongitude(Double.valueOf(jsonObject.getString("longitude")));
                            museums.add(museumTampon);


                            //send the string of title to the main thread
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("title", museumTampon.getTitle());
                            bundle.putString("latitude", museumTampon.getLatitude());
                            bundle.putDouble("longitude", museumTampon.getLongitude());
                            msg.setData(bundle);
                            ListMuseeActivity.this.UIhandler.sendMessage(msg);


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                }
            }
        });
    }



    //to create the list in the main thread
    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            final String title = bundle.getString("title");
            final String latitude = bundle.getString("latitude");
            final  Double longitude = bundle.getDouble("longitude");
            //create the table automatically on the screen
            TableRow row = new TableRow(ListMuseeActivity.this);
            Button button = new Button(ListMuseeActivity.this);
            TextView txtview = new TextView(ListMuseeActivity.this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            button.setText(title);
            txtview.setText(title);
            System.out.println(title);
            row.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent itMusee = new Intent(ListMuseeActivity.this, MuseeActivity.class);
                    itMusee.putExtra("title", title);
                    itMusee.putExtra("latitude", latitude);
                    itMusee.putExtra("longitude", longitude);
                    startActivity(itMusee);
                }
            });
            //row.addView(txtview);

            tb.addView(row);

        }
    }
}
