package com.android.shen.miniprojet_shen_xinyue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.shen.miniprojet_shen_xinyue.dao.VisitBDD;
import com.android.shen.miniprojet_shen_xinyue.dto.Visit;

import java.util.List;

public class VisitActivity extends AppCompatActivity {
    TableLayout tableLayout;
    VisitBDD visitBDD;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);



    }

    public void createList(){
        TableRow row = null;
        visitBDD = new VisitBDD(this);
        visitBDD.open();
        List<Visit> visits = visitBDD.getVisits();


        for (Visit visit: visits){
            //create the table automatically on the screen
            row = new TableRow(VisitActivity.this);
            TextView tvVisit = new Button(VisitActivity.this);
            TextView tvDate = new TextView(VisitActivity.this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvVisit.setText(visit.getTitle());
            tvDate.setText(String.valueOf(visit.getDate()));

            row.addView(tvVisit);
            row.addView(tvDate);
        }


        tableLayout = findViewById(R.id.list_visit);
        tableLayout.addView(row);
    }


}
