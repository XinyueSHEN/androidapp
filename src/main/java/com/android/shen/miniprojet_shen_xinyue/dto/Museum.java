package com.android.shen.miniprojet_shen_xinyue.dto;

public class Museum {
    private int id;
    private String title;
    private String description;
    private String tarifs;
    private String latitude;
    //private double latitude;
    private double longitude;

    public Museum() {
    }

    public Museum(int id, String title, String description, String tarifs, String latitude, int longitude) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.tarifs = tarifs;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTarifs() {
        return tarifs;
    }

    public void setTarifs(String tarifs) {
        this.tarifs = tarifs;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Museum{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", tarifs='" + tarifs + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
