package com.android.shen.miniprojet_shen_xinyue.dto;

public class Person {
    private int id;
    private String nom;
    private String prenom;
    //private String tel;
    //private String ville;
    private int nbMusee;
    private String email;
    private String password;
    //0 for user and 100 for admin
    private int userType;
    private String photo;



    public Person() {
    }

    public Person(int id, String nom, String prenom, int nbMusee, String email, String password, int userType, String photo) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.nbMusee = nbMusee;
        this.email = email;
        this.password = password;
        this.userType = userType;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /*public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
*/
    public int getNbMusee() {
        return nbMusee;
    }

    public void setNbMusee(int nbMusee) {
        this.nbMusee = nbMusee;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", nbMusee=" + nbMusee +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userType=" + userType +
                ", photo='" + photo + '\'' +
                '}';
    }
}
