package com.android.shen.miniprojet_shen_xinyue;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.shen.miniprojet_shen_xinyue.dto.Museum;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AfficherActivity extends AppCompatActivity implements OnMapReadyCallback {
    private List<Museum> museums;
    private TableLayout tb;
    private AfficherActivity.UIHandler UIhandler;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_afficher);

        UIhandler = new AfficherActivity.UIHandler();

        //get the list of the museums from the link in Json
        getFromJsonList();


    }

    protected void getFromJsonList() {
        //create the list of museums
        museums = new ArrayList<>();
        //list of museums
        tb = findViewById(R.id.list);


        //make http request
        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder()
                .url("http://srvinfodev.esigelec.fr/musee.json")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String webContent = response.body().string();
                if (!response.isSuccessful()){
                    throw new IOException("Erreur: " +response);
                } else {
                    //create JSONObject to parse JSON to Java
                    //String str = new String("{ \"number\": [3, 4, 5, 6] }");
                    try {
                        JSONArray jsonarray = new JSONArray(new String(webContent));
                        JSONObject jsonObject;
                        for (int i = 0; i < jsonarray.length(); i++){
                            //get the list of museums from the Json file
                            jsonObject = jsonarray.getJSONObject(i);
                            Museum museumTampon = new Museum();
                            museumTampon.setTitle(jsonObject.getString("title"));
                            museumTampon.setDescription(jsonObject.getString("description"));
                            museumTampon.setTarifs(jsonObject.getString("tarifs"));
                            //museumTampon.setLatitude(Double.valueOf(jsonObject.getString("latitude")));
                            museumTampon.setLatitude(jsonObject.getString("latitude"));
                            museumTampon.setLongitude(Double.valueOf(jsonObject.getString("longitude")));
                            museums.add(museumTampon);


                            //send the string of title to the main thread
                            Message msg = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString("title", museumTampon.getTitle());
                            bundle.putString("latitude", museumTampon.getLatitude());
                            bundle.putDouble("longitude", museumTampon.getLongitude());
                            msg.setData(bundle);
                            AfficherActivity.this.UIhandler.sendMessage(msg);


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                }
            }
        });
    }



    //to create the list in the main thread
    private class UIHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int i = 0;
            if(i != 0){
                super.handleMessage(msg);
                Bundle bundle = msg.getData();
                final String title = bundle.getString("title");
                final String latitude = bundle.getString("latitude");
                final  Double longitude = bundle.getDouble("longitude");

                System.out.println(title);


                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
                mapFragment.getMapAsync(AfficherActivity.this);
                /*Uri uri = Uri.parse("geo: "+Double.valueOf(latitude)+", "+ longitude +"?z=18");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
                */
            }
            i++;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //get the location form the mainActivity
        Intent intent = getIntent();
        Double la = intent.getDoubleExtra("latitude", 0);
        Double lo = intent.getDoubleExtra("longitude", 0);


        // Add a marker in the car itinerary
        if (la != null & lo != null) {
            LatLng carIti = new LatLng(la, lo);
            // Add polylines and polygons to the map.
            Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                    .clickable(true)
                    .add(new LatLng(-35.016, 143.321),
                            new LatLng(-34.747, 145.592),
                            new LatLng(-34.364, 147.891),
                            new LatLng(-33.501, 150.217),
                            new LatLng(-32.306, 149.248),
                            new LatLng(-32.491, 147.309)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-23.684, 133.903), 4));
            //mMap.addPolyline()
            //mMap.addMarker(new MarkerOptions().position(carIti).title("car itinerary"));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(carIti));
        } else {
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }
    }
}
