package com.android.shen.miniprojet_shen_xinyue;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.shen.miniprojet_shen_xinyue.dao.PersonBDD;
import com.android.shen.miniprojet_shen_xinyue.dto.Person;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ProfilActivity extends AppCompatActivity implements LocationListener {
    TextView getNom;
    TextView getPrenom;
    TextView getTel;
    TextView getVille;
    TextView getNbMusee;
    PersonBDD personBDD;
    Intent itMain;
    Geocoder geocoder;
    private static final int PERMISSION_GPS = 100;
    private static final int PERMISSION_PHONE = 1;
    private LocationManager locationManager;
    private Location location;
    private String locationProvider;
    private List<String> providerList;
    private Address address;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        ////check the internet status
        checkInternet();

        //initialize location and get location permission
        getVille = findViewById(R.id.ville);
        locationInit();

        //get phone number
        if (!checkPermission(Manifest.permission.READ_PHONE_STATE)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)){
                Toast.makeText(this, "Phone state permission allows us to get phone number. Please allow it for additional functionality.", Toast.LENGTH_LONG).show();
            }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_PHONE);
        } else {
            getTel = findViewById(R.id.tel);
            getTel.setText(getPhone());

        }

        //get the intent from the main page
        itMain = getIntent();

        //get the user info from bdd
        getBddInfo();

    }

    private String getPhone() {
        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        return phoneMgr.getLine1Number();
    }

    private boolean checkPermission(String permission){
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }






    public void checkInternet(){
        //check the internet status
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo == null)
            System.out.println("Pas de réseaux");
        else
            System.out.println(networkInfo.getTypeName() + " " + networkInfo.getState());

    }






    //get the user info from bdd
    public void getBddInfo() {
        personBDD = new PersonBDD(this);
        personBDD.open();
        //Person perBDD = personBDD.getPersonParNom("Tom");
        Person perBDD = personBDD.getPersonParNom(itMain.getStringExtra("getnom"));

        if (perBDD != null && perBDD.getNom()!= null && perBDD.getPrenom()!=null) {
            getNom = findViewById(R.id.nom);
            getNom.setText(perBDD.getNom());
            getPrenom = findViewById(R.id.prenom);
            getPrenom.setText(perBDD.getPrenom());
            getVille.setText(address.getLocality());
            getNbMusee = findViewById(R.id.nbMusee);
            getNbMusee.setText(String.valueOf(perBDD.getNbMusee()));
        }
        else
            System.out.println("parameter in  profil is null");
    }








    //result of the location access permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_GPS){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ProfilActivity.this);
            } else
                Toast.makeText(this, "Permission refusee", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == PERMISSION_PHONE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                System.out.println(getPhone());
            } else {
                Toast.makeText(this,"Permission Denied. We can't get phone number.", Toast.LENGTH_LONG).show();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }









    //initialize location
    public void locationInit() {
        try {
            // get system service
            locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            providerList = locationManager.getProviders(true);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_GPS);
            } else
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ProfilActivity.this);


            // check if GPS could get the location
            // check by GPS then network
            if (locationInitByGPS() || locationInitByNETWORK()) {
                // show location
                address = getAddress(getApplicationContext(),location.getLatitude(), location.getLongitude());
            } else {
                getVille.setText("failed to get location");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // use GPS to get location
    public boolean locationInitByGPS() {
        List<String> providerList = locationManager.getProviders(true);
        if (providerList.contains(LocationManager.GPS_PROVIDER)) {
            System.out.println("=====GPS_PROVIDER=====");
            locationProvider = LocationManager.GPS_PROVIDER;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

            location = locationManager.getLastKnownLocation(locationProvider);
        }
        if (location != null) {
            return true;
        } else {
            return false;
        }
    }

    // use network to get location
    public boolean locationInitByNETWORK() {
        if (providerList.contains(LocationManager.NETWORK_PROVIDER)){
            System.out.println("=====NETWORK_PROVIDER=====");
            locationProvider = LocationManager.NETWORK_PROVIDER;
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
            location = locationManager.getLastKnownLocation(locationProvider);
        }

        if (location != null) {
            return true;
        } else {
            return false;
        }
    }

    public Address getAddress(Context context, double latitude, double longitude) {
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
