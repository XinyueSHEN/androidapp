package com.android.shen.miniprojet_shen_xinyue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.shen.miniprojet_shen_xinyue.dao.PersonBDD;
import com.android.shen.miniprojet_shen_xinyue.dto.Person;

public class RegisterActivity extends AppCompatActivity {
    EditText nom;
    EditText prenom;
    EditText email;
    EditText pwd;
    PersonBDD personBDD;
    Button btnRegis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //pass from loginActivity to RegisterActivity
        Intent itLogin = getIntent();

        //register list
        email = findViewById(R.id.etMail);
        email.setText(itLogin.getStringExtra("email"));
        pwd = findViewById(R.id.etMDP);
        pwd.setText(itLogin.getStringExtra("pwd"));
        nom = findViewById(R.id.etNom);
        prenom = findViewById(R.id.etPrenom);

        //register all the information entered
        bddRegister();

    }

    public void bddRegister() {
        personBDD = new PersonBDD(this);
        btnRegis = findViewById(R.id.register_button);
        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create a new person
                Person person = new Person();
                person.setPrenom(prenom.getText().toString());
                person.setNom(nom.getText().toString());
                person.setEmail(email.getText().toString());
                person.setPassword(pwd.getText().toString());
                person.setNbMusee(0);
                //person.setVille("Rouen");
                //person.setTel("0618268261");
                person.setUserType(100);
                personBDD.open();

                personBDD.ajouterPerson(person);
                Person perBDD = personBDD.getPersonParNom(nom.getText().toString());
                //Person perBDD = personBDD.getPersonParNom("SHEN");
                if (perBDD != null) {
                    Toast.makeText(RegisterActivity.this, perBDD.toString(),Toast.LENGTH_LONG).show();
                }

                //guide to the Index Page
                Intent itMain = new Intent(RegisterActivity.this, MainActivity.class);
                itMain.putExtra("nbMusee", person.getNbMusee());
                itMain.putExtra("nom", person.getNom());
                itMain.putExtra("photo", perBDD.getPhoto());
                itMain.putExtra("prenom", person.getPrenom());
                itMain.putExtra("usertype", person.getUserType());
                startActivity(itMain);
            }
        });
    }
            }
